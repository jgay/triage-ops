# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class RequestLegalReview < Processor
    FEATURE_LABELS = [
      'type::feature',
      'feature::addition'
    ].freeze

    APPLICABLE_WORKFLOW_LABELS = [
      'workflow::design',
      'workflow::solution validation',
      'workflow::planning breakdown',
      'workflow::ready for development',
      'workflow::in dev',
      'workflow::in review',
      'workflow::verification',
      'workflow::awaiting security release'
    ].freeze

    LEGAL_LABEL = 'legal issue created'
    LEGAL_COMPLIANCE_PROJECT_ID = 13363506

    react_to 'issue.open', 'issue.update', 'issue.reopen'

    def applicable?
      event.from_part_of_product_project? &&
        a_feature_issue? &&
        applicable_workflow_labels_present? &&
        legal_label_not_added?
    end

    def process
      Triage.api_client.post(create_legal_issue_path, body: legal_review_issue_payload)
      add_comment(%(/label ~"#{LEGAL_LABEL}"), append_source_link: false)
    end

    def documentation
      <<~TEXT
        Creates a legal review issue for feature::addition issues as part of the product dev process.
      TEXT
    end

    private

    def a_feature_issue?
      (FEATURE_LABELS - event.label_names).empty?
    end

    def applicable_workflow_labels_present?
      (event.label_names & APPLICABLE_WORKFLOW_LABELS).any?
    end

    def legal_label_not_added?
      !event.label_names.include?(LEGAL_LABEL)
    end

    def create_legal_issue_path
      "/projects/#{LEGAL_COMPLIANCE_PROJECT_ID}/issues"
    end

    def legal_review_issue_payload
      {
        title: legal_review_issue_title,
        description: legal_review_issue_description
      }
    end

    def event_issue_web_url
      "#{event.project_web_url}/issues/#{event.iid}"
    end

    def legal_review_issue_title
      "Legal Review Questionnaire: #{event.title}"
    end

    def legal_review_issue_description
      <<~MARKDOWN.chomp
        # Product Feature and Version Development - Legal Review Questionnaire (LRQ)

        <!--
        - Answer questions with 'y' (yes), 'n' (no), or 'n/a' (not applicable).
        - When answering 'y' or 'n/a', include further details to aid the review process.

        The gating question determines if you need to complete the full questionnaire.
        If you're unsure about how to answer, review the full questionnaire for additional context, or reach out in the #legal Slack channel for guidance.
        -->

        ### **Product Issue and gating question**
        **Link to the Product Issue in which the proposed solution is being discussed:** #{event_issue_web_url}
        <!--paste a link to the Product Issue in which the proposed solution is being discussed-->

        ### **Does the proposed solution involve any changes that will have an impact on:**

        - the licensing of third-party software or dependencies;
        - trademarks or patents;
        - global trade compliance (or involve use of any new encryption algorithms or new key lengths);
        - data privacy including capturing consent or changes to the collection or use of personal data of users or others; or
        - anything else that should be raised with the Legal & Corporate Affairs team?

        <!--
        If 'no' to ALL of the above, state this, and create the issue without answering any further questions. No further action is required at this time.

        If 'yes' to ANY of the above, state which, and answer all remaining questions below.
        -->

        ---

        ### **1. Dependency Software Licenses**

        - 1. Does the proposed solution create a **new** dependency on a third-party project that is subject to a potentially acceptable (see bit.ly/3aEhwbo), unacceptable (see bit.ly/3uSLgbE), or proprietary license?

            -

        ### **2. Intellectual Property**

        - 1. Does the proposed solution:

                - a. Intend to be given a distinctive (rather than descriptive) name? See naming guidelines at bit.ly/3zbsjn2

                    -

                - b. Involve the invention of a new and useful process that may be eligible for disclosure under the GitLab Patent Program (see bit.ly/3PirI8z)

                    -

        ### **3. Global Trade Compliance**

        - 1. Does the proposed solution involve the use of any new encryption algorithms or new key lengths?

            -

        - 2. If so, do those algorithms (including the protocol and underlying crypto modules) leverage non-standard cryptography. Non-standard Cryptography is defined at bit.ly/3aE9kYH.

            -

        - 3. Does the proposed solution:

                - a. Incorporate an open cryptographic interface? Open Cryptographic Interface is defined at bit.ly/3aMHY2B

                    -

                - b. Involve (i) new pre-processing methods (e.g., data  compression or data interleaving) that are applied to plaintext data prior to encryption; or (ii) new post-processing methods (e.g., packetization, encapsulation) applied to the cipher text after encryption?

                    -

                - c. Support any new communication protocols (e.g., X.25, Telnet, or TCP)?

                    -

                - d. Involve the use of any compilers, runtime interpreters, or code assemblers, other than Ruby 2.4.4, Go 1.10, and Python 3?

                    -

        ### **4. Data Privacy**

        - 1. Does the proposed solution:

            - a. Involve the use of personal data? See definition in GitLab's Privacy Policy at about.gitlab.com/privacy

                -

            - b. Involve the collection of personal data not currently provided for in the GitLab Privacy Policy?

                -

            - c. Involve the use of personal data already permitted for collection under the GitLab Privacy Policy but for a new purpose that isn’t covered within the Policy?

                -

            - d. Permit the user to opt-out of the collection and use of their personal data? If not, explain why?.

                -

            - e. Involve the implementation of any new cookies, tracking pixels, or beacons? If yes, provide details.

                -

            - f. Involve any change to Service Ping (see bit.ly/3oaQ6Nm) or Snowplow (see bit.ly/3IFOcxL) data collection?

                -

            - g. Involve any change to the categorization of Services Usage Data (see bit.ly/3aJyNA3) as 'Subscription', 'Optional', or 'Operational'? If yes, provide details.

                -

            - h. Involve AI, machine learning, data matching from multiple sources, automated decision-making that could have a significant impact on individuals, or processing of biometric data like fingerprints or facial recognition? If yes, provide details.

                -

        - 2. If personal data were not collected and used in the proposed solution, would GitLab function as normal albeit without the new functionality of the proposed solution?

            -

        ### **5. Anything else**

        - 1. Does the proposed solution concern anything else that should be raised with the Legal & Corporate Affairs Team?

            -

        /label ~"Product Dev - Legal Review Questionnaire"
        /confidential
      MARKDOWN
    end
  end
end
